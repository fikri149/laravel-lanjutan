<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pinjaman extends Model
{
    protected $table = 'pinjaman';

    protected $fillable = [
        'tgl_pinjam', 'tgl_batas', 'tgl_kembali', 'ontime', 'mahasiswa_id', 'buku_id'
    ];

    public function Mahasiswa()
    {
        return $this->belongsTo('App\Mahasiswa');
    }

    public function Buku()
    {
        return $this->belongsTo('App\Buku');
    }
}
