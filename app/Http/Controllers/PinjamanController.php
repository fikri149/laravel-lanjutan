<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pinjaman;

class PinjamanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Pinjaman::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tgl_pinjam' => ['required', 'date'],
            'tgl_batas' => ['required', 'date'],
            'tgl_kembali' => ['required', 'date'],
            'ontime' => ['required', 'boolean'],
            'mahasiswa_id'=> ['required', 'integer'],
            'buku_id' => ['required', 'integer']
        ]);

        Pinjaman::create([
            'tgl_pinjam' => $request->tgl_pinjam,
            'tgl_batas' => $request->tgl_batas,
            'tgl_kembali' => $request->tgl_kembali,
            'ontime' => $request->ontime,
            'mahasiswa_id'=> $request->mahasiswa_id,
            'buku_id' => $request->buku_id
        ]);

        return response("Data Pinjaman tersimpan");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pinjaman = Pinjaman::find($id);

        $pinjaman->update(['ontime' => $request->ontime]);

        return response('status pinjaman telah diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
