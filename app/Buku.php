<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table = 'bukus';

    protected $fillable = [
        'kode_buku', 'judul', 'pengarang', 'tahun_terbit'
    ];

    public function Pinjaman()
    {
        return $this->hasMany('App\Pinjaman');
    }
}
