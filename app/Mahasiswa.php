<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    protected $table = 'mahasiswas';

    protected $fillable = [
        'nama', 'nim', 'fakultas', 'jurusan', 'no_hp', 'no_wa', 'user_id'
    ];

    public function Pinjaman()
    {
        return $this->hasMany('App\Pinjaman');
    }

    public function User()
    {
        return $this->belongsTo('App\User');
    }
}
