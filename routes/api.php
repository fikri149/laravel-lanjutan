<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Auth')->group(function() {
    Route::post('register', 'RegisterController');
    Route::post('login', 'LoginController');
    Route::post('logout', 'LogoutController');
});

Route::middleware('auth:api')->group(function() {
    Route::get('mahasiswa', 'MahasiswaController@index');

    Route::get('buku', 'BukuController@index');
    Route::get('pinjaman', 'PinjamanController@index');
    Route::post('pinjaman', 'PinjamanController@store');
});

Route::middleware(['auth:api', 'roles'])->group(function() {
    Route::get('user', 'UserController');
    Route::post('buku', 'BukuController@store');
    Route::post('mahasiswa', 'MahasiswaController@store');
    Route::put('pinjaman/{pinjaman}', 'PinjamanController@update');
});

